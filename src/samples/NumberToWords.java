package samples;

public class NumberToWords {

	public void pw(Integer num, String word) {

		String one[] = { " ", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", 
		
		                 " Nine", " Ten", " Eleven", " Twelve", " Thirteen", " Fourteen", "Fifteen", 
		                 
		                 " Sixteen", " Seventeen", " Eighteen", " Nineteen" };

		String ten[] = { " ", " ", " Twenty", " Thirty", " Forty", " Fifty", " Sixty", "Seventy",
		
		              " Eighty", " Ninety" };

		if ( num > 19 ) {

			System.out.print( ten[num / 10] + " "  + one[num % 10] );
		}
		else {

			System.out.print( one[num] );
		}
		
		if ( num > 0 )

			System.out.print( word );
	}

	public static void main(String[] args) {

		Integer num = 12345;

		System.out.print(num);

		if ( num <= 0 ) {

			System.out.println(" Enter numbers greater than 0");
		}
		else {

			NumberToWords a = new NumberToWords();

		//	a.pw( ( num / 1000000000 ), " Hundred");  // TODO :: Need to check

			a.pw( ( num / 10000000 ) % 100, " crore");

			a.pw( (num / 100000 ) % 100, " lakh");

			a.pw( ( num / 1000 ) % 100, " thousand");

			a.pw( ( num / 100 ) % 10, " hundred");

			a.pw( ( num % 100 ), " ");
		}
	}
}