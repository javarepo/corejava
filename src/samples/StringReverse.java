package samples;

 public class StringReverse {
	

	public static void main(String[] args) {
		
		String str = "sample";
		
		char ch[] = str.toCharArray(); 
		
		StringBuilder stringBuilder = new StringBuilder(); 
		
		for ( int i = ch.length -1 ; i >= 0  ; i -- ) {
			
			stringBuilder.append(ch[i]); 
			
		}
		
		System.out.println(stringBuilder.toString());
		
	}

}
