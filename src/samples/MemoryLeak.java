package samples;

import java.util.Map;


public class MemoryLeak {

	public final String key;

	public MemoryLeak(String key) {

		this.key = key;
	}


	public static void main(String args[]) {

		try {

			Map<Object, Object> map = System.getProperties();

			for ( ; ; )  {

				map.put(new MemoryLeak("one"), "onjj");

				System.out.print(map);
			}
		} 

		catch(Exception e) {

			e.printStackTrace();
		}
	}
}
