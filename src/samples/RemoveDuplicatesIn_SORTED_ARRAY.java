package samples;

public class RemoveDuplicatesIn_SORTED_ARRAY {

	public static void main(String[] args) {

		int arr[] = { -1 ,  -1 , -5 ,100 , 200 , 100 , 10,10,10,10,10,20,20,30}; 

		removeDuplicates(arr);

	}

	private static void removeDuplicates( int[] arr ) {

		int count = 1 ;

		for ( int i = 1; i < arr.length; i++ ) {

			int temp = arr[i];

			if( temp != arr[i-1] ) {

				arr[ count ++ ] = temp ;

			}
		}

		for( int i = 0 ; i < count ; i ++ ) {

			System.out.print(arr[i] + " " );

		}

	}

}