package samples;

import java.util.HashMap;
import java.util.Map;

public class OccurenceInString {

	public static void main(String[] args) {

		String str = "wwelcomew";

		Map<Character, Integer> map = new HashMap<Character, Integer>();

		for (char ch : str.toCharArray()) {

			if ( map.containsKey(ch) ) {

				int val = map.get(ch);

				map.put(ch, val + 1  );

			} else {

				map.put(ch, 1);
			}
		}

		System.out.println(map);
	}
}

