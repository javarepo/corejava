package custom_annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.METHOD ,ElementType.FIELD , ElementType.TYPE } )
@Inherited
@Documented
public @interface MyAnnotation {
	
	public int value();
	
	int value1() default 0; 
	
	String value3();
	
	String value4();
	

}
