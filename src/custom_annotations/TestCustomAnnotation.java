package custom_annotations;

import java.lang.reflect.Method;

//@MyAnnotation(value = 10, value3 = "ABC", value4 = "XYZ")
public class TestCustomAnnotation {
	
	@MyAnnotation(value = 333, value3 = "ABC", value4 = "XYZ")
	public void sayHello() {
		
		System.out.println("sayHello method");
		
	}
	
	
	public static void main(String args[]) throws NoSuchMethodException, SecurityException {
		
		TestCustomAnnotation obj = new TestCustomAnnotation();
		
		Method method = obj.getClass().getMethod("sayHello");
		
		MyAnnotation myAnnoation = method.getAnnotation(MyAnnotation.class);
		
		System.out.println( "Value :: " + myAnnoation.value3());
		
		
	}

}

