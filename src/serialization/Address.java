package serialization;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;

public final class Address implements Externalizable {

	private static final long serialVersionUID = 1L;

	private  String street; // Blank final variable
	
	private   String country; // Blank static final variable

	private static String stateCode ; 
	
	/*static {
		
		country = "INDIA";
	}
	*/
	
public Address() {
		
		System.out.println("no arg constructor ");
	}
	
	/*Address(String street , String country ) {
		
		this.street = street;
		
		 this.country = country;
	}*/

	public void setStreet(String street){
		this.street = street;
	}

	public void setCountry(String country){
		this.country = country;
	}

	public String getStreet(){
		return this.street;
	}

	public String getCountry(){
		return this.country;
	}

	@Override
	public String toString() {
		return new StringBuffer(" Street : ")
		.append(this.street)
		.append(" Country : ")
		.append(this.country)
		.append(" State Code : ")
		.append(this.stateCode).
		toString();
	}

	
	
	
	public static String getStateCode() {
		return stateCode;
	}

	public static void setStateCode(String stateCode) {
		Address.stateCode = stateCode;
	}

	/*public Object readResolve() throws ObjectStreamException {
		
		System.out.println("readResolve() method");

		return this;
	}*/
	
	@Override
	public void readExternal(ObjectInput arg0) throws IOException,ClassNotFoundException {
		
		street = (String)arg0.readObject();
		
		country = (String)arg0.readObject();
		
		System.out.println("readExternal() method");
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {

		out.writeObject(street);

		out.writeObject(country);

		System.out.println("writeExternal() method");
	}

	/*private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {

		
		System.out.println("readObject() method");
	}*/
	
	private void writeObject(Object paramObject) throws IOException {

		
		System.out.println("writeObject() method");
	}

}