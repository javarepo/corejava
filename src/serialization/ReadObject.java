package serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
 
public class ReadObject{  // Deserialization
	
private static String jsonFilePath = System.getProperty("user.dir") + File.separator + "src"+ File.separator +"com"+ File.separator +"test"+ 
			
			File.separator + "serialization" + File.separator;
	
 
   public static void main (String args[]) {
 
	   Address address;
	   
	   try  {
 
		   FileInputStream fin = new FileInputStream(jsonFilePath+"address.txt");
		   
		   ObjectInputStream ois = new ObjectInputStream(fin);
		   
		   address = (Address) ois.readObject();
		   
		   ois.close();
 
		   System.out.println(address);
		   
	   }catch(Exception ex){
		   
		   
		   ex.printStackTrace(); 
	   } 
   }
}
