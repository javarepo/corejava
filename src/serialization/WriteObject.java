package serialization;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
 
public class WriteObject{  // Serialilzation
	
	
	private static String jsonFilePath = System.getProperty("user.dir") + File.separator + "src"+ File.separator +"com"+ File.separator +"test"+ 
			
			File.separator + "serialization" + File.separator;
	
 
	public static void main (String args[]) {
	 
	   Address address = new Address();
	   address.setStreet("wall street");
	   address.setCountry("united states");
	   address.setStateCode("ABC");
	   
	  // Address address2 = new Address();
 
	   try{
 
		FileOutputStream fout = new FileOutputStream(jsonFilePath+"address.txt");
		
		ObjectOutputStream oos = new ObjectOutputStream(fout);   
		
		oos.writeObject(address);
		
		oos.close();
		
		//System.out.println(address2.getStreet());
		
		//System.out.println(address2.getCountry());
		
		
		System.out.println("Done");
 
	   }catch(Exception ex){
		   ex.printStackTrace();
	   } 
	}
}
