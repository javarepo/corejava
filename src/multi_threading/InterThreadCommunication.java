package multi_threading;

class Customer {

	int amount = 10000;  

	synchronized void withdraw(int amount){

		System.out.println("going to withdraw...");  

		if ( this.amount < amount ) {

			System.out.println("Less balance; waiting for deposit...");

			try {

				wait();

			}

			catch( Exception e )

			{


			}  
		}  

		this.amount-=amount;
		
		// notify(); 

		System.out.println("withdraw completed...");  
	}  

	synchronized void deposit(int amount){

		System.out.println("going to deposit...");
		
		/*try {

			wait();

		}

		catch( Exception e )

		{


		}  
*/
		this.amount+=amount;

		System.out.println("deposit completed... ");

		notify();  
	}  
}  

class InterThreadCommunication {

	public static void main(String args[]) throws InterruptedException {

		final Customer c = new Customer();

		Thread t1 = new Thread(){

			public void run(){

				c.withdraw(15000);

			}

		};

		

		Thread t2 = new Thread(){

			public void run(){

				c.deposit(10000);

			}

		}; 

		//t2.start(); // Deposit
		
		t1.start(); // Withdraw
		
		// To make sure that t2 finishes first before t1  
		
		t2.join(); // Deposit
		
		t1.join(); // Withdraw
		
		 

	}}  

